#include <Applications/Application/app.hxx>
#include <UI/Widgets/mainwindow.hxx>

#include "config.hxx"

using namespace DEMO_NAMESPACE_NAME;

int main(int argc, char** argv) {
    QCoreApplication::setApplicationName(QString::fromStdString(applicationName));
    QCoreApplication::setApplicationVersion(QString::fromStdString(applicationVersion));
    QCoreApplication::setOrganizationName(QString::fromStdString(organizationName));
    QCoreApplication::setOrganizationDomain(QString::fromStdString(organizationDomain));
    QApplication::setApplicationDisplayName(QObject::tr("%1 V. %2 by %3")
                                            .arg(QCoreApplication::applicationName())
                                            .arg(QCoreApplication::applicationVersion())
                                            .arg(QCoreApplication::organizationName()));

    PROJECT_NAMESPACE_NAME::APP_NAMESPACE_NAME::App a(argc, argv);
    PROJECT_NAMESPACE_NAME::WIDGETS_NAMESPACE::MainWindow w;

    w.show();

    return a.exec();
}
