#pragma once

#include <Applications/Application/databaseaction.hxx>

PROJECT_NAMESPACE_BEGIN
APP_NAMESPACE_BEGIN

class ActionForAvailableDatabase : public DatabaseAction
{
public:
    explicit ActionForAvailableDatabase(const QString& text, QObject* p = nullptr, const QKeySequence& seq = {});

public slots:
    void onDatabaseAvailable();
    void onDatabaseUnavailable();
};

APP_NAMESPACE_END
PROJECT_NAMESPACE_END
