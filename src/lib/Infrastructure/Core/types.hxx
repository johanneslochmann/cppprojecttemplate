#pragma once

#include <Infrastructure/Core/config.hxx>

#include <string>
#include <vector>
#include <sstream>

PROJECT_NAMESPACE_BEGIN

using String = std::string;
using StringVector = std::vector<String>;
using StringStream = std::stringstream;

PROJECT_NAMESPACE_END
