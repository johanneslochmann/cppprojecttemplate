#include <catch.hpp>

#include <infrastructure/core/tools.hxx>

USE_PROJECT_NAMESPACE

SCENARIO("string case conversion") {
    GIVEN("a string in all caps") {
        WHEN("converting it to lower case") {
            THEN("it is lower cased") {
              REQUIRE("foo bar" == toLowerCase("FOO BAR"));
            }
        }
    }
    GIVEN("a mixed case string") {
        WHEN("converting it to lower case") {
            THEN("it is lower cased") {
                REQUIRE("foo bar" == toLowerCase("Foo BaR"));
            }
        }
    }
    GIVEN("a lower case string") {
        WHEN("converting it to lower case") {
            THEN("it is still lower cased") {
                REQUIRE("foo bar" == toLowerCase("foo bar"));
            }
        }
    }
}

SCENARIO("replace a substring in a string") {
    GIVEN("A string") {
        WHEN("replacing a substring in it") {
            THEN("the substring is replaced") {
                REQUIRE("a big lamb" == replace("a small lamb", "small", "big"));
            }
        }
    }
}

