#pragma once

#include <UI/Widgets/intedit.hxx>

PROJECT_NAMESPACE_BEGIN
WIDGETS_NAMESPACE_BEGIN

class PortEdit : public IntEdit
{
    Q_OBJECT
public:
    using IntEdit::IntEdit;
};

WIDGETS_NAMESPACE_END
PROJECT_NAMESPACE_END
