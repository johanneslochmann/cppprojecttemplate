#pragma once

#include <UI/Widgets/label.hxx>

PROJECT_NAMESPACE_BEGIN
WIDGETS_NAMESPACE_BEGIN

class StatusLabel : public Label
{
public:
    using Label::Label;
};

WIDGETS_NAMESPACE_END
PROJECT_NAMESPACE_END
