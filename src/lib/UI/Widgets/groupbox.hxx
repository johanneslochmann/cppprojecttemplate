#pragma once

#include <QtWidgets/QGroupBox>

#include <UI/Widgets/config.hxx>

PROJECT_NAMESPACE_BEGIN
WIDGETS_NAMESPACE_BEGIN

class GroupBox : public QGroupBox
{
    Q_OBJECT
public:
    using QGroupBox::QGroupBox;
};

WIDGETS_NAMESPACE_END
PROJECT_NAMESPACE_END
