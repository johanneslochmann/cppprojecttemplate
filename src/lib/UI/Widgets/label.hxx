#pragma once

#include <UI/Widgets/config.hxx>

#include <QLabel>

PROJECT_NAMESPACE_BEGIN
WIDGETS_NAMESPACE_BEGIN

class Label: public QLabel
{
    Q_OBJECT
public:
    using QLabel::QLabel;
};

WIDGETS_NAMESPACE_END
PROJECT_NAMESPACE_END
