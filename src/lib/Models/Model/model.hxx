#pragma once

#include <Models/Model/config.hxx>

PROJECT_NAMESPACE_BEGIN
MODEL_NAMESPACE_BEGIN

class Model
{
public:
    Model();
    virtual ~Model();
};

MODEL_NAMESPACE_END
PROJECT_NAMESPACE_END
