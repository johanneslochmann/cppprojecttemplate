#include <catch.hpp>

#include <models/model/model.hxx>

USE_PROJECT_NAMESPACE
USE_MODEL_NAMESPACE

SCENARIO("instantiate a model") {
    WHEN("instantiating a model") {
        THEN("no assertion is thrown") {
            Model m;
            (void) m;
        }
    }
}
