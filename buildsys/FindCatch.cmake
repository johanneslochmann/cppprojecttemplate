#
# This module finds the 3rdparty Catch library
#
# CATCH_FOUND - Set to true if Catch and headers are found. Otherwise False or undefined.
# CATCH_INCLUDE_DIR - Where Catch Header files are located
#
FIND_PATH(CATCH_INCLUDE_DIR
    catch.hpp
    HINTS 3rdparty/Catch
    PATH_SUFFIXES "include"
)

IF (CATCH_INCLUDE_DIR)
    MESSAGE(STATUS "Catch include files found in: ${CATCH_INCLUDE_DIR}")
ELSE (CATCH_INCLUDE_DIR)
    MESSAGE(FATAL_ERROR "Catch include files NOT found.")
ENDIF (CATCH_INCLUDE_DIR)

IF (CATCH_INCLUDE_DIR)
    SET(CATCH_FOUND TRUE)
ENDIF (CATCH_INCLUDE_DIR)
